var gameConfig = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 2000 },
      debug: false
    }
  },
  scene: {
    preload: preload,
    create: create,
    update: update
  }
};

function preload()
{
  this.load.image('sky', 'src/assets/sprites/sky.png');
  this.load.image('ground', 'src/assets/sprites/platform.png');
  this.load.image('star', 'src/assets/sprites/star.png');
  this.load.image('bomb', 'src/assets/sprites/bomb.png');
  this.load.spritesheet('dude', 
      'src/assets/sprites/dude.png',
      { frameWidth: 32, frameHeight: 44 }
  );
  this.load.audio('jump', ['src/assets/audio/jump.wav', 'src/assets/audio/jump.ogg']);
  this.load.audio('eat', ['src/assets/audio/eat.wav', 'src/assets/audio/eat.ogg']);
  this.load.audio('die', ['src/assets/audio/die.wav', 'src/assets/audio/die.ogg']);
  this.load.audio('respawn', ['src/assets/audio/respawn.wav', 'src/assets/audio/respawn.ogg']);
  this.load.audio('release', ['src/assets/audio/release.wav', 'src/assets/audio/release.ogg']);
}

function create()
{
  // Add background
  this.add.image(400, 300, 'sky');

  // Add platforms
  platforms = this.physics.add.staticGroup();
  platforms.create(400, 568, 'ground').setScale(2).refreshBody();
  platforms.create(600, 400, 'ground');
  platforms.create(50, 260, 'ground');
  platforms.create(750, 220, 'ground');
  if (Math.random() >= 0.9)
  {
    platforms.create(715, 300, 'ground');
  }
  else
  {
    platforms.create(620, 300, 'ground').setScale(0.7).refreshBody();
  }
  
  // Add player
  player = this.physics.add.sprite(100, 450, 'dude');
  player.setBounce(0.2);
  player.setCollideWorldBounds(true);
  
  this.anims.create({
    key: 'left',
    frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3}),
    frameRate: 10,
    repeat: -1
  });
  
  this.anims.create({
    key: 'turn',
    frames: [ { key: 'dude', frame: 4 } ],
    frameRate: 24
  });
  
  this.anims.create({
    key: 'right',
    frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 } ),
    frameRate: 10,
    repeat: -1
  });
  
  cursors = this.input.keyboard.createCursorKeys();
  
  // Add stars
  stars = this.physics.add.group({
    key: 'star',
    repeat: 11,
    setXY: { x: 12, y: 0, stepX: 70 }
  });
  stars.children.iterate(function (child) {
    child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
  });
  
  // Add title
  titleText = this.add.text(16, 16, 'Kimchi Kountdown', { fill: '#e03', font: '20pt Cursive' });
  
  // Add score text
  highScoreText = this.add.text(16, 56, 'High:  ' + highScore, { fill: '#a50', font: '20pt Monospace' });
  scoreText = this.add.text(16, 80, 'Score: 0', { fill: '#a50', font: 'bold 20pt Monospace' });
  
  // Add bombs
  bombs = this.physics.add.group();

  // Add colliders
  this.physics.add.collider(platforms, player);
  this.physics.add.collider(stars, platforms);
  this.physics.add.collider(bombs, platforms);
  this.physics.add.collider(player, bombs, hitBomb, null, this);
  this.physics.add.overlap(player, stars, collectStar, null, this);
}

function update ()
{
  if (gameOver)
  {
    return;
  }
  
  if (cursors.left.isDown)
  {
    player.setVelocityX(-300);

    player.anims.play('left', true);
  }
  else if (cursors.right.isDown)
  {
    player.setVelocityX(300);

    player.anims.play('right', true);
  }
  else
  {
    player.setVelocityX(0);

    player.anims.play('turn');
  }

  if (cursors.up.isDown && player.body.touching.down)
  {
    player.setVelocityY(-800);
    this.sound.play('jump');
  }
}

function restart()
{
  bombs.children.iterate(function (child) {
    child.disableBody(true, true);
  });
  stars.children.iterate(function (child) {
    child.enableBody(true, child.x, 0, true, true);
  });
  player.setTint(0xffffff);
  player.setPosition(100, 450);
  updateScore(0);
  gameOver = false;
  this.sound.play('respawn');
  this.physics.resume();
}

function collectStar (player, star)
{
  star.disableBody(true, true);

  updateScore(score + 10);
  
  this.sound.play('eat');
  
  if (stars.countActive(true) === 0)
  {
    stars.children.iterate(function (child) {
      child.enableBody(true, child.x, 0, true, true);
    });
    releaseBomb();
    this.sound.play('release');
  }
}

function updateScore (newScore)
{
  score = newScore;
  scoreText.setText('Skore: ' + score);
  if (score > highScore)
  {
    highScore = score;
    highScoreText.setText('High:  ' + highScore);
    localStorage.setItem('hiscore', highScore);
  }
}

function releaseBomb()
{
  var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

  var bomb = bombs.create(x, 16, 'bomb');
  
  bomb.setBounce(1);
  bomb.setCollideWorldBounds(true);
  bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
  bomb.allowGravity = false;
}

function hitBomb (player, bomb)
{
  bomb.disableBody(true, true);
  this.physics.pause();
  this.sound.play('die');
  player.setTint(0x000000);
  player.anims.play('turn');
  gameOver = true;
  timedEvent = this.time.addEvent({
    delay: 1500,
    callback: restart,
    callbackScope: this
  });
}

var highScore = 0;
if (localStorage.getItem('hiscore') !== null)
{
  highScore = parseInt(localStorage.getItem('hiscore'));
}
var gameOver = false;
var score = 0;
var scoreText;
var game = new Phaser.Game(gameConfig);