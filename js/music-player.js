var audio = {    
    init: function() {        
      var $that = this;        
      $(function() {            
          $that.components.media();        
      });  
    },
    components: {        
        media: function(target) {            
            var media = $('audio.player', (target !== undefined) ? target : 'body');            
            if (media.length) {                
                media.mediaelementplayer({                    
                    audioHeight: 40,
                    features : ['playpause', 'current', 'duration', 'progress', 'volume', 'tracks', 'fullscreen'],
                    alwaysShowControls      : true,
                    timeAndDurationSeparator: '<span></span>',
                    iPadUseNativeControls: true,
                    iPhoneUseNativeControls: true,
                    AndroidUseNativeControls: true
                });            
            }        
        },            
    },
};
audio.init();

$(".music-player a").each(function() {
  this.title = "Download";
});