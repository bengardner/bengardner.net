(function($) {
  "use strict"; // Start of use strict

  var portfolioSeen = false;
  
  // Closes the sidebar menu
  $(".menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
    $(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times");
    $(this).toggleClass("active");
  });

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 750, "easeOutQuint");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('#sidebar-wrapper .js-scroll-trigger').click(function() {
    $("#sidebar-wrapper").removeClass("active");
    $(".menu-toggle").removeClass("active");
    $(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times");
  });

  $(document).scroll(function() {
    checkPortfolio();
    // Scroll to top button appear
    if ($(this).scrollTop() > 100) {
      $('.scroll-to-top').stop().fadeIn();
    } else {
      $('.scroll-to-top').stop().fadeOut();
    }
  });
  
  // Shows portfolio when button clicked
  $('.btn-portfolio').click(showPortfolio);

  function checkPortfolio()
  {
    if ($(document).scrollTop() + $(window).height() > $('#portfolio').offset().top + 100){
      showPortfolio();
    }    
  }
  
  function showPortfolio()
  {
    if (!portfolioSeen)
    {
      portfolioSeen = true;
      $('#portfolio').animate({
        opacity: 1,/* 
        marginTop: '-=15',
        marginBottom: '+=15' */
      }, {duration: 'slow', queue: false});
    }
  }
  
  $(document).ready(function() {
    // Reset video on modal close
    $("#modal-skyworld").on('hidden.bs.modal', function () {
      $("#modal-skyworld iframe").attr("src", $("#modal-skyworld iframe").attr("src"));
    });
    
    $('#portfolio').css('opacity', '0');
    checkPortfolio();
  });

})(jQuery); // End of use strict

// Disable Google Maps scrolling
// See http://stackoverflow.com/a/25904582/1607849
// Disable scroll zooming and bind back the click event
var onMapMouseleaveHandler = function(event) {
  var that = $(this);
  that.on('click', onMapClickHandler);
  that.off('mouseleave', onMapMouseleaveHandler);
  that.find('iframe').css("pointer-events", "none");
}
var onMapClickHandler = function(event) {
  var that = $(this);
  // Disable the click handler until the user leaves the map area
  that.off('click', onMapClickHandler);
  // Enable scrolling zoom
  that.find('iframe').css("pointer-events", "auto");
  // Handle the mouse leave event
  that.on('mouseleave', onMapMouseleaveHandler);
}
// Enable map zooming with mouse scroll when the user clicks the map
$('.map').on('click', onMapClickHandler);